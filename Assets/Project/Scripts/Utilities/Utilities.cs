﻿using UnityEngine;

namespace Utility
{
    public static class Utilities
    {
        /// <summary>
        /// Shuffle an array.
        /// </summary>
        /// <typeparam name="T">Array Type</typeparam>
        /// <param name="array">Array to shuffle</param>
        public static void Shuffle<T>(ref T[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                T temp = array[i];
                int randomIndex = Random.Range(i, array.Length);
                array[i] = array[randomIndex];
                array[randomIndex] = temp;
            }
        }

        /// <summary>
        /// Load from asset path but throw exception if null is returned. 
        /// </summary>
        /// <typeparam name="T"> Class </typeparam>
        /// <param name="path"> Path in RESOURCE folder </param>
        /// <returns>Return array of T object.</returns>
        public static T[] LoadAllFromPath<T>(string path) where T : UnityEngine.Object
        {
            var assets = Resources.LoadAll<T>(path);

            if (assets != null)
                return assets;

            throw new System.ArgumentNullException(path + " could not be found.");
        }

        public static T LoadFromPath<T>(string path) where T : UnityEngine.Object
        {
            var assets = Resources.Load<T>(path);

            if (assets != null)
                return assets;

            throw new System.ArgumentNullException(path + " could not be found.");
        }
    }
}
