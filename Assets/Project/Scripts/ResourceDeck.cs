﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceDeck : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    /// METHOD 1
    /// Get Drop event of resource card from deck to trash/hand
    /// ONLY IF PLAYER DROP THE CARD IN TRASH OR HAND, deck must 
    /// interrogate pullmanager, create a new resourceCard and 
    /// place under the one already showed up
    
    /// METHOD 2
    /// Get Drop event and only if it go to trash, deck must set 
    /// that card to a "null state" and reinsert into pull manager
}
