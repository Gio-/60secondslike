﻿using UnityEngine;

[System.Serializable]
public class RequestedValue
{
    [SerializeField]
    public int ID;
    [SerializeField]
    public int Value;
}

[System.Serializable]

[CreateAssetMenu(fileName = "Event Card", menuName = "Event Card/New Event Card", order = 1)]
public class EventCard : ScriptableObject
{

    [SerializeField]
    public string title;
    [SerializeField]
    [Multiline(5)]
    public string desc;
    [SerializeField]
    public int id;

    [SerializeField]
    public string image;

    [SerializeField]
    public RequestedValue[] requestedResources;
}
