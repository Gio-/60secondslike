﻿using Utility;

public class DeckModel {

    private static DeckModel instance = null;

    public ResourceCard[] resourceDeck;
    public EventCard[] eventDeck;

    /// <summary>
    /// With this, we ensure that none can create multiple
    /// instance of this and access to class only through
    /// getInstance().
    /// </summary>
    public DeckModel()
    {
    }

    public static DeckModel getInstance()
    {
        if (instance == null)
            instance = new DeckModel();

        return instance;
    }

    /// Create a destroyer  

    /// Create a method to get and store all resources cards
    public void GetAllResourcesCards(ref ResourceCard[] cards, string path)
    {
        try
        {
            cards = Utilities.LoadAllFromPath<ResourceCard>(path);
        }
        catch (System.ArgumentException e)
        {
            UnityEngine.Debug.LogError(e);
            UnityEngine.Debug.Break();
        }
    }

    public void GetAllEventCard(ref EventCard[] cards, string path)
    {
        try
        {
            cards = Utilities.LoadAllFromPath<EventCard>(path);
        }
        catch (System.ArgumentException e)
        {
            UnityEngine.Debug.LogError(e);
            UnityEngine.Debug.Break();
        }
    }

    public ResourceCard GetRandomResourceCard()
    {
        int weightSum = 0;
        int i = 0;

        for (i = 0; i < resourceDeck.Length; i++)
            weightSum += resourceDeck[i].probability;

        int rand = UnityEngine.Random.Range(0, weightSum);
        int top = 0;
        
        for (i = 0; i < resourceDeck.Length; i++)
        {
            top += resourceDeck[i].probability;
            if (rand < top)
            {
                i = resourceDeck.Length + 1;
                return resourceDeck[i];
            }
        }

        return null;
    }

    /// Create a method to get all unlocked events cards

    /// Create a method to get all events cards from a particular set
}
