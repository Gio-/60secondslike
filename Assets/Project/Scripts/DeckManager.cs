﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility;

public class DeckManager : MonoBehaviour {

    [Header("Card Resources")]
    public ResourceCard[] deck;
    [Space(10)]
    [Header("Card Events")]
    public EventCard[] deckEvent;
    
	void Start () {
       Utilities.Shuffle(ref deck);
       Utilities.Shuffle(ref deckEvent);
	}
	
    T[] CreateDeck<T>(T[] card, int maxCard, bool makeEqual)
    {
        return new T[10];
    }
}
