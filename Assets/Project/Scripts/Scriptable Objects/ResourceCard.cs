﻿using UnityEngine;

[CreateAssetMenu(fileName = "Resource Card", menuName = "Resource Card/New Resource Card", order = 1)]
public class ResourceCard : ScriptableObject {

    [Header("Card ID")]
    /// <summary>
    /// Resource Card ID
    /// </summary>
    public int id;

    [Space(20)]

    [Header("Card Bio")]
    /// <summary>
    /// Resource Card Title
    /// </summary>
    public string title;

    /// <summary>
    /// Resource Card Description
    /// </summary>
    [Multiline(5)]
    public string desc;

    [Space(20)]

    [Header("Card Spec")]
    /// <summary>
    /// Resource Card Draw Probability
    /// </summary>
    [Range(1, 100)]
    public int probability = 1;

    /// <summary>
    /// Resource Card Value
    /// </summary>
    [Range(1, 1000)]
    public int value = 1;

    [Space(20)]

    [Header("Card Image")]
    /// <summary>
    /// Resouce Card Image 
    /// </summary>
    public string image;
}
